const mysql = require('mysql');

const Sequelize = require('sequelize');

/**
  SETUP Your DB Connection
*/

var conn = mysql.createConnection({
  host: process.env.DB_HOST,
  port: process.env.DB_PORT,
  user: process.env.DB_USERNAME,
  password: process.env.DB_PASSWORD,
  database: process.env.DB_DATABASE
});

conn.connect((err) => {
  if(!err){
    console.log('DB Connected Successfully...');
  }else {
    console.log('DB Connected Failed \n Error : ' + JSON.stringify(err, undefined, 2));
  }
});

let db_slave = new Sequelize(process.env.DB_DATABASE, process.env.DB_USERNAME, process.env.DB_PASSWORD, {

    host: process.env.DB_HOST,
    port: process.env.DB_PORT,
    dialect: process.env.DB_MYSQL,

    pool: {
      max: 30,
      min: 0,
      idle: 10000
    },
    define: {
          timestamps: false
      }

  });

  db_slave
  .authenticate()
  .then(() => {
    console.log('Connection Slave has been established successfully.');
  })
  .catch(err => {
    console.error('Unable to connect to the database:', err);
  });


global.Sequelize = Sequelize

module.exports = {
  conn: conn,
  db_slave: db_slave
};
