

class AuthTokenMiddle{

    async checkToken(req, res, next){
        try{
            // check header or url parameters or post parameters for token
            var token = req.headers['token'];

            // decode token
            if (token) {
                let tokenData = token.split('Bearer ')[1].trim();
                // verifies
                next();

            }else{
                return res.status(400).json({
                    success: 0, message: 'Authorization Code required.'
                })
            }

        }catch(error){
            return res.status(500).json({
                success: 0, message: 'Something went Wrong.'
            })
        }
    }

}

module.exports  = new AuthTokenMiddle();
