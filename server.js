const http = require('http');
const dotenv = require('dotenv');
dotenv.config();

const port = process.env.APP_PORT;

const app = require('./app');

const server = http.createServer(app);

server.listen(port,()=>{
  console.log('server listen at port '+ port)
});
